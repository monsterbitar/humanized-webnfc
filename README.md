# Introduction

The [`webnfc`](https://www.npmjs.com/package/webnfc) library provides a simple and easy to user framework for reading and writing NFC tags using WebNFC.

## Features

- can check if the device supports and allows NFC usage.
- makes it easy to request NFC permission.
- can read and write to NFC tags.
- handles edge-cases like duplicate reads.
- idempotent function which can be called at any time
- lots of events to provide good insight into inner workings.

For information on all typings, arguments and options available, read the [documentation](https://monsterbitar.gitlab.io/webnfc/) generated from the source code.

## My device has support for NFC, it is enabled but your library doesn't see it!

WebNFC is only supported in a small number of mobile browsers, and is only supported when used in a `secure context`.

While developing your application you might not serve it under HTTPS, in which case you need to aks your browser nicely to support WebNFC despite being in an insecure context.

### Chrome

Go to `chrome://flags/#unsafely-treat-insecure-origin-as-secure` and add an exception to `localhost` or the `IP-number and port` you want to use while developing.

# Usage

## Installation

Installation is easy, just get the library from NPM:

```sh
npm install webnfc
```

## Setup

```ts
// Import library features.
import
{
	deviceSupportsNFC,
	deviceAllowsNFC,
	requestAccessToNFC,
	nfcEvents,
	listenForTags,
	readFromTag,
	stopReadingFromTags,
	writeToTag,
	stopWritingToTag
} from 'webnfc';

// Check if device supports WebNFC.
if(deviceSupportsNFC())
{
	// Great, the device does support WebNFC.
}
```


## Requesting permission

Prior to reading or writing NFC tags, you need to ensure that the device is allowed to use NFC.

```ts
// Check if device allows WebNFC usage.
if(!deviceAllowsNFC())
{
	// No permission at this time.
}
```

If device does not have the appropriate permission, you need to request the permission **from a user interaction**, such as a button click.

```ts
// Locate a suitable button or some such in your document..
const myButton = document.querySelector('#myButton') as HTMLElement;

// .. then add an event listener that requests permission when user clicks the button.
myButton.addEventListener('click', requestAccessToNFC);
```


## Event handling

When you interact with NFC tags, events will be emitted with various things like the status of the device, but also information on the tags being read or written to.

### Detecting permission

After performing a permission request, you will either be granted or denied permission.

```ts
// Log when permission is granted.
nfcEvents.on('PermissionGranted', console.log);

// Log when permission is denied.
nfcEvents.on('PermissionDenied', console.log);
```

### Detecting current state

While you perform various NFC related actions, the library will emit events indicating it's current state.

```ts
// Log when device is ready to read tags.
nfcEvents.on('ReadStart', console.log);

// Log when device is no longer ready to read tags.
nfcEvents.on('ReadStop', console.log);

// Log when device is ready to write to a tag.
nfcEvents.on('WriteStart', console.log);

// Log when device is no longer ready to write to a tag.
nfcEvents.on('WriteStop', console.log);
```

### Reading events

```ts
// Log when device has read a new tag.
nfcEvents.on('TagDetected', console.log);

// Log when device has ignored a tag that was read in duplicate.
nfcEvents.on('TagIgnored', console.log);

// Log the tag records when the device has read a new tag.
nfcEvents.on('TagRecords', console.log);
```

### Writing events

Currently, there are no event on successfully writing to a tag.


## Reading tags

Before you can read any tag, you must first first set up your event listened for the `TagDetected` or `TagRecords` events.

### Reading a single tag.

To read a single tag, simple call `readFromTag()`.

```ts
await readFromTag(timeoutInSeconds);
```

### Continuously reading tags

If you want to read multiple tags, use `listenForTags()`.

NOTE: If you set the timeout to 0, it will listen continuously until turned off.

```ts
await listenForTags(timeoutInSeconds)
```

### Stop reading tags

If you are listening for tags but want to stop, use `stopReadingFromTags()`.

```ts
await stopReadingFromTags(someReason);
```

## Writing tags

### Writing a single tag

To write a tag, simply call `writeToTag()` with the data you want to write.

You will need to provide a `mimeType` (for example text/plain) and encode your data into an ArrayBuffer prior to calling.

By default the library will not overwrite tags, but you can set `overwrite` to true and existing data will be replaced.

NOTE: Tags that has an empty record are not empty tags, and needs overwrite to be overwritten. You can format cards to fully erase them.

```ts
await writeToTag(mimeType, data, overwrite, timeoutInSeconds);
```


### Writing to multiple tags

This has not yet been implemented, but may come in a future version.


### Stop writing to tags

If you are currently trying to write to a tag, but want to stop, you can call `stopWritingToTag()`.

```ts
await stopWritingToTag();
```
