// Import library to manage event signals.
import { EventEmitter } from 'events';

// Import type information.
import type { NDEFReader, NDEFReadingEvent, NDEFRecord, NDEFWriteOptions } from './interfaces.ts';

// Set up a variable to hold the initialized NDEFReader from the browser.
let nfcTag: NDEFReader;

// Set up timers for scan and write events.
let scanTimeout: ReturnType<typeof setTimeout>;
let writeTimeout: ReturnType<typeof setTimeout>;

// Set up initial scan and write states.
let isScanning: boolean = false;
let isWriting: boolean = false;

// Set up abort controllers.
let scanController: AbortController;
let writeController: AbortController;

// Set up a tracker for the last read serial number to prevent multiple reads from triggering.
let lastReadSerialNumber: string;
let lastReadTimeout: ReturnType<typeof setTimeout>;

// Configure how long we reject the same tag to prevent duplicate reading in single human interaction.
const lastReadCooldownDurationInMilliseconds = 2500;

/**
 * The 'TagDetected' event is emitted when a new tag has been read, and provides the tag serial number as the only parameter.
 * @event
 */
export type TagDetected = string;

/**
 * The 'TagIgnored' event is emitted when the same tag has been read multiple times and provides the tag serial number as the only parameter.
 * @event
 */
export type TagIgnored = string;

/**
 * The 'TagRecords' event is emitted when a new tag has been read, and provides an array of NDEF records.
 * @event
 *
 * @note full description for the NDEFRecord type is available in the exported interfaces.
 */
export type TagRecords = Array<NDEFRecord>;

/**
 * the 'ReadStart' event is emitted when the device begins listening for nearby tags.
 * @event
 */
export type ReadStart = undefined;

/**
 * the 'ReadStop' event is emitted when the device stops listening for nearby tags.
 * @event
 *
 * @note the event may provide a string explaining why it has stopped.
 */
export type ReadStop = string;

/**
 * the 'WriteStart' event is emitted when the device is ready to write to the next nearby device.
 * @event
*/
export type WriteStart = undefined;

/**
 * the 'WriteStop' event is emitted when the device is no longer ready to write to the next nearby device.
 * @event
 *
 * @note the event may provide a string explaining why it has stopped.
 */
export type WriteStop = string;

/**
 * the 'PermissionGranted' event is emitted if the device allows access after performing a permission request.
 * @event
 */
export type PermissionGranted = undefined;

/**
 * the 'PermissionDenied' event is emitted if the device does not allow access after performing a permission request.
 * @event
 */
export type PermissionDenied = undefined;

/**
 * Emits events upon successful or failed NFC interactions.
 *
 * @group EventEmitter
 */
export const nfcEvents: EventEmitter = new EventEmitter();

/**
 * Utility function that returns true if the current device supports NFC.
 * @group Utilities
 */
export const deviceSupportsNFC = async function(): Promise<boolean>
{
	// Return true if the NFC capabilities are present in the window object.
	return ('NDEFReader' in window);
};

/**
 * Utility function that returns true if the current device allows NFC usage.
 * @group Utilities
 */
export const deviceAllowsNFC = async function(): Promise<boolean>
{
	// Request NFC permission state from the browser.
	// @ts-ignore
	const nfcPermissionStatus = await navigator.permissions.query({ name: 'nfc' });

	// Return true if permission has been granted.
	return (nfcPermissionStatus.state === 'granted');
};

/**
 * Utility function to request permission to use the devices NFC capabilities.
 * @group Utilities
 *
 * @note this will initiate scanning for NFC tags and then immediately stop the scan.
 */
export const requestAccessToNFC = async function(): Promise<void>
{
	// Instantiate a web NFC reader, if necessary.
	if(!nfcTag)
	{
		// @ts-ignore
		nfcTag = new NDEFReader();
	}

	// Set up a new abort controller for the permissions request.
	const requestAbortController = new AbortController();

	// Request scanning in order to trigger permission request.
	await nfcTag.scan({ signal: requestAbortController.signal });

	// Abort the request as we had no intention to actually scan a tag.
	await requestAbortController.abort('completed permission request');

	if(deviceAllowsNFC())
	{
		nfcEvents.emit('PermissionGranted');
	}
	else
	{
		nfcEvents.emit('PermissionDenied');
	}
};

/**
 * Utility function that returns true if the device is currently trying to read NFC tags.
 * @group Reading
 */
export const isReadingTags = async function(): Promise<boolean>
{
	return isScanning;
};

/**
 * Utility function that returns true if the device is currently trying to write to an NFC tag.
 * @group Writing
 */
export const isWritingTags = async function(): Promise<boolean>
{
	return isWriting;
};

/**
 * Stops reading NFC tags.
 * @group Reading
 *
 * @param reason - explanation for why we stopped reading tags.
 */
export const stopReadingFromTags = async function(reason?: string): Promise<void>
{
	// Ignore this call if we are not currently scanning.
	if(!isScanning)
	{
		return;
	}

	// Cancel any scan timeouts.
	if(scanTimeout)
	{
		// Clear the timeout.
		clearTimeout(scanTimeout);

		// Reset the tracker.
		scanTimeout = undefined;
	}

	// Stop scanning tags.
	await scanController.abort(reason);

	// Update the state tracker.
	isScanning = false;

	// Reset the last read serial timeout and serial number, so we can read the same tag again later.
	clearTimeout(lastReadTimeout);
	lastReadSerialNumber = undefined;

	// Emit an event indicating that we stopped scanning.
	nfcEvents.emit('ReadStop', reason);
};

/**
 * Stops writing to NFC tags.
 * @group Writing
 *
 * @param reason - explanation for why we stopped writing to tags.
 */
export const stopWritingToTag = async function(reason?: string): Promise<void>
{
	// Ignore this request if we are not currently writing.
	if(!isWriting)
	{
		return;
	}

	// Cancel any scan timeouts.
	if(writeTimeout)
	{
		clearTimeout(writeTimeout);
	}

	// Stop scanning tags.
	writeController.abort(reason);

	// Update the state tracker.
	isWriting = false;

	// Emit an event indicating that we stopped writing.
	nfcEvents.emit('WriteStop', reason);
};

/**
 * Utility function that stops any ongoing read or write operation.
 */
const stopDeviceOperation = async function(reason?: string): Promise<void>
{
	await stopReadingFromTags(reason);
	await stopWritingToTag(reason);
};

/**
 * Utility function to unset the last read serial number.
 */
const clearLastReadSerialNumber = async function(): Promise<void>
{
	lastReadSerialNumber = undefined;
};

/**
 * Utility function that parses a tag and emits relevant events.
 * TODO: reconsider event names and additional events.
 */
const parseTag = async function(event: NDEFReadingEvent): Promise<void>
{
	// Deconstruct the event for legibility.
	const { serialNumber, message } = event;

	// Deconstruct the records for legibility.
	const { records } = message;

	// Ignore this tag if we have already read it.
	if(serialNumber === lastReadSerialNumber)
	{
		// Emit a tag ignored event to indicate that we did indeed read a card.
		nfcEvents.emit('TagIgnored', serialNumber);

		// Skip processing the card we just read.
		return;
	}

	// Store this tag as the last read serial number.
	lastReadSerialNumber = serialNumber;

	// Clear last read timeout if available.
	clearTimeout(lastReadTimeout);

	// Set up a timer that clears the last read serial number after three seconds.
	lastReadTimeout = setTimeout(clearLastReadSerialNumber, lastReadCooldownDurationInMilliseconds);

	// Emit human friendly events.
	nfcEvents.emit('TagDetected', serialNumber);
	nfcEvents.emit('TagRecords', records);
};

/**
 * Utility function that parses a tag and emits relevant events, then stops reading tags.
 */
const parseTagThenStop = async function(event: NDEFReadingEvent): Promise<void>
{
	// Parse the tag.
	await parseTag(event);

	// Cancel any existing operation.
	await stopDeviceOperation('completed read operation');
};

/**
 * Listens for NFC tags and emits events when successfully reading a tag.
 * @group Reading
 *
 * @note this also stops any currently running read or write operations.
 *
 * @param timeoutInSeconds - if set to non-zero, stop scanning after this many seconds.
 * @param stopAfterOneTag - if set to true, stop reading after one tag interaction.
 */
export const listenForTags = async function(timeoutInSeconds: number = 0, stopAfterOneTag: boolean = false): Promise<void>
{
	// Instantiate a web NFC reader, if necessary.
	if(!nfcTag)
	{
		// @ts-ignore
		nfcTag = new NDEFReader();
	}

	// Hook up an event emitter on read tags.
	nfcTag.onreading = (stopAfterOneTag ? parseTagThenStop : parseTag);

	// TODO: Handle errors.
	// nfcTag.onreadingerror = console.log;

	// Cancel any existing operation.
	await stopDeviceOperation('starting read operation');

	// Set up a new abort controller for this operation.
	scanController = new AbortController();

	// Enable scanning of NFC tags, with an abort controller so it can be disabled later.
	await nfcTag.scan({ signal: scanController.signal });

	// Update state tracker to indicate we are now scanning.
	isScanning = true;

	// Emit an event indicating that we started scanning.
	nfcEvents.emit('ReadStart');

	// Set up timer to cancel after indicated time.
	if(timeoutInSeconds)
	{
		scanTimeout = setTimeout(stopReadingFromTags.bind(null, 'read operation timed out'), timeoutInSeconds * 1000);
	}
};

/**
 * Reads data from the next tag interaction.
 * @group Reading
 *
 * @note this also stops any currently running read or write operations.
 *
 * @param timeoutInSeconds - if set to non-zero, stop reading after this many seconds.
 */
export const readFromTag = async function(timeoutInSeconds: number = 0): Promise<void>
{
	// Listen for one tag, then stop.
	return listenForTags(timeoutInSeconds, true);
};

/**
 * Writes data to the next tag interaction.
 * @group Writing
 *
 * @note this also stops any currently running read or write operations.
 * @note tags with empty records on them does hold data, and will require the overwrite flag.
 *
 * @param mimeType         - a string containing the desired mime type to write.
 * @param data             - a piece of binary data to write to the tag.
 * @param overwrite        - if set to true, will overwrite tags with existing data.
 * @param timeoutInSeconds - if set to non-zero, stop writing after this many seconds.
 */
export const writeToTag = async function(mimeType: string, data: ArrayBuffer, overwrite: boolean = false, timeoutInSeconds: number = 0): Promise<void>
{
	// Instantiate a web NFC reader, if necessary.
	if(!nfcTag)
	{
		// @ts-ignore
		nfcTag = new NDEFReader();
	}

	// Cancel any existing operation.
	await stopDeviceOperation('starting write operation');

	// Set up a new abort controller for this operation.
	writeController = new AbortController();

	// Only write to empty cards.
	const options: NDEFWriteOptions =
	{
		overwrite,
		signal: writeController.signal,
	};

	// Structure the backup record.
	const record =
	{
		recordType: 'mime',
		mediaType: mimeType,
		data: data,
	};

	// Set up the list of records as the message to record.
	const message =
	{
		records: [ record ],
	};

	// Write the backup.
	const writePromise = nfcTag.write(message, options);

	// Update state tracker to indicate we are now writing.
	isWriting = true;

	// Emit an event indicating that we started writing.
	nfcEvents.emit('WriteStart');

	// Set up timer to cancel after indicated time.
	if(timeoutInSeconds)
	{
		writeTimeout = setTimeout(stopWritingToTag.bind(null, 'write operation timed out'), timeoutInSeconds * 1000);
	}

	// Return the write promise.
	return writePromise;
};
